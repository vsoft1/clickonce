﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceInstall
{
    class Program
    {
        private static readonly string SERVICE_APP_NAME = "TopShelfClickOnceTest.exe";

        static bool Stop()
        {
            var pInfo = new ProcessStartInfo(SERVICE_APP_NAME, "stop");
            pInfo.UseShellExecute = true;
            pInfo.Verb = "runas";
            var process = Process.Start(pInfo);            
            process.WaitForExit();

            return process.ExitCode == 0;
        }

        static bool Uninstall()
        {
            var pInfo = new ProcessStartInfo(SERVICE_APP_NAME, "uninstall");
            pInfo.UseShellExecute = true;
            pInfo.Verb = "runas";
            var process = Process.Start(pInfo);
            process.WaitForExit();

            return process.ExitCode == 0;
        }

        static bool Install()
        {
            var pInfo = new ProcessStartInfo(SERVICE_APP_NAME, "install");
            pInfo.UseShellExecute = true;
            pInfo.Verb = "runas";
            var process = Process.Start(pInfo);
            process.WaitForExit();

            return process.ExitCode == 0;
        }

        static bool Start()
        {
            var pInfo = new ProcessStartInfo(SERVICE_APP_NAME, "start");
            pInfo.UseShellExecute = true;
            pInfo.Verb = "runas";
            var process = Process.Start(pInfo);
            process.WaitForExit();

            return process.ExitCode == 0;
        }

        static void Main(string[] args)
        {
            if (Stop())
                Uninstall();

            if (Install())
            {
                Start();
                Console.Write("Serviço instalado e iniciado com sucesso! Presssione <ENTER> para fechar.");
            }
            else
                Console.Write("Erro ao instalar serviço! Contate o Suporte Técnico! Presssione <ENTER> para fechar.");

            Console.ReadLine();
        }
    }
}
